package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    @Unroll
    def "initial balance #balance1 #balance2 and money to transfer #moneyToTransfer should return #expectedBalance1 #expectedBalance2 #expectedAnswer"() {

        given: "initial data"
            def bank = new Bank()

        when: "when money is transferred"
            def accountNumber1 = bank.createAccount()
            def accountNumber2 = bank.createAccount()

            bank.deposit(accountNumber1,balance1)
            bank.deposit(accountNumber2,balance2)

            def isTransferred = bank.transfer(accountNumber1,accountNumber2,moneyToTransfer)

        then: "check if money was transferred correctly"
            expectedBalance1 == bank.accountBalance(accountNumber1)
            expectedBalance2 == bank.accountBalance(accountNumber2)
            isTransferred == expectedAnswer

        cleanup:
            bank.deleteAccount(accountNumber1)
            bank.deleteAccount(accountNumber2)

        where:
            balance1 | balance2 | moneyToTransfer | expectedBalance1 | expectedBalance2 | expectedAnswer
            200 | 100 | 50 | 150 | 150 | true
            100 | 100 | 100 | 0 | 200 | true
            100 | 50 | 101 | 100 | 50 | false



    }

    @Unroll
    def "should return false for non existent account"() {

        given: "bank with 1 account"
            def bank = new Bank()
            def accountNumber = bank.createAccount()
            def nonExistentAccount = accountNumber+1
            bank.deposit(accountNumber,300)

        when: "money is transferred"
            def isTransferred1 = bank.transfer(accountNumber,nonExistentAccount,100)
            def isTransferred2 = bank.transfer(nonExistentAccount,accountNumber,100)

        then: "check if money was transferred correctly"
            isTransferred1 == false
            isTransferred2 == false
    }
}
