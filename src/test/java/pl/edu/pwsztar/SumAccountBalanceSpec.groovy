package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountBalanceSpec extends Specification{

    @Unroll
    def "should return sum of balance of all existent account == 1000"() {

        given: "new Bank with 4 accounts"
            def bank = new Bank()
            def accountNumber1 = bank.createAccount()
            def accountNumber2 = bank.createAccount()
            def accountNumber3 = bank.createAccount()
            def accountNumber4 = bank.createAccount()

            bank.deposit(accountNumber1,400)
            bank.deposit(accountNumber2,300)
            bank.deposit(accountNumber3,200)
            bank.deposit(accountNumber4,100)

        when: "money is added up"
            def sum = bank.sumAccountsBalance()

        then: "check if sum equals 1000"
            sum == 1000

        cleanup:
            bank.deleteAccount(accountNumber1)
            bank.deleteAccount(accountNumber2)
            bank.deleteAccount(accountNumber3)
            bank.deleteAccount(accountNumber4)
    }

    @Unroll
    def "should return 0 for empty Bank"() {

        given: "new Bank with no accounts"
        def bank = new Bank()

        when: "money is added up"
            def sum = bank.sumAccountsBalance()

        then: "check if sum equals 0"
            sum == 0

    }
}
