package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification{

    @Unroll
    def "should deposit 100 on new account and return true"() {

        given: "new empty account"
            def bank = new Bank()
            def accountNumber = bank.createAccount()

        when: "money is deposited"
            def isDeposited = bank.deposit(accountNumber,100)
            def balance = bank.accountBalance(accountNumber)

        then: "check if money was deposited correctly"
            isDeposited == true
            balance == 100
    }

    @Unroll
    def "should return false for non existent account"() {

        given: "new Bank with no accounts"
            def bank = new Bank()

        when: "money is deposited"
            def isDeposited = bank.deposit(100,100)

        then: "check if money was deposited correctly"
           isDeposited == false

    }
}
