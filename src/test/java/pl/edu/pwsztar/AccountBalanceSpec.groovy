package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountBalanceSpec extends Specification{

    @Unroll
    def "should return balance == 0 for new account "(){
        given: "new empty account"
            def bank = new Bank()
            int accountNumber = bank.createAccount()

        when: "the account balance is checked"
            def balance = bank.accountBalance(accountNumber)

        then: "check if balance == 0"
            balance == 0

        cleanup:
            bank.deleteAccount(accountNumber)
    }

    @Unroll
    def "should return balance == -1 for non existent account"(){
        given: "new Bank with no accounts"
            def bank = new Bank()

        when: "the account balance is checked"
            def balance = bank.accountBalance(1)

        then: "check if balance == -1"
            balance == -1
    }
}
