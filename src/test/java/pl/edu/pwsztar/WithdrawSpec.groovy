package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification{

    @Unroll
    def "should withdraw #withdraw from account with balance #balance and return #expectedBalance and #expectedAnswer"() {

        given: "initial data"
            def bank = new Bank()

        when: "when money is withdrawn"
            def accountNumber = bank.createAccount()
            bank.deposit(accountNumber,balance)
            def isWithdrawn = bank.withdraw(accountNumber,withdraw)

        then: "check if money was withdrawn correctly"
            expectedBalance == bank.accountBalance(accountNumber)
            isWithdrawn == expectedAnswer

        cleanup:
            bank.deleteAccount(accountNumber)

        where:
            balance | withdraw | expectedBalance | expectedAnswer
            200 | 100 | 100 | true
            50 | 50 | 0 | true
            50 | 51 | 50 | false


    }

    @Unroll
    def "should return false for non existent account"() {

        given: "new Bank with no accounts"
            def bank = new Bank()

        when: "money is withdrawn"
            def isWithdrawn = bank.withdraw(1,100)

        then: "check if money was withdrawn correctly"
            isWithdrawn == false

    }
}
