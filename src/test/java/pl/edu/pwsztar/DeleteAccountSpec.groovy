package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should return balance == 0 for new account"(){
        given: "new empty account"
            def bank = new Bank()
            int accountNumber = bank.createAccount()

        when: "the account is deleted"
            def balance = bank.deleteAccount(accountNumber)

        then: "check if balance == 0 "
            balance == 0
    }

    @Unroll
    def "should return balance == -1 for non existent account"(){
        given: "new Bank with no accounts"
            def bank = new Bank()

        when: "the account is deleted"
            def balance = bank.deleteAccount(1)

        then: "check if balance == -1 "
            balance == -1
    }
}
