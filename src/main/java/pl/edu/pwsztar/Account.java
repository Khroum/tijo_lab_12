package pl.edu.pwsztar;


import lombok.Getter;

@Getter
public class Account {
    private final int number;
    private int balance;

    public Account(int number) {
        this.number = number;
        balance = 0;
    }

    void deposit(int amount){
        balance+=amount;
    }

    boolean withdraw(int amount){
        if (amount > balance){
            return false;
        }

        balance-=amount;
        return true;
    }

    boolean transfer(Account destination, int amount){
        if (amount>balance){
            return false;
        }

        withdraw(amount);
        destination.deposit(amount);
        return true;
    }

}
