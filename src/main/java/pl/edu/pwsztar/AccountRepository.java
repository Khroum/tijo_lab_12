package pl.edu.pwsztar;

import java.util.List;

public interface AccountRepository {
    Account getByNumber(int accountNumber) throws AccountNonExistentException;

    void create(Account account);

    void delete(int accountNumber) throws AccountNonExistentException;

    List<Account> getAll();
}
