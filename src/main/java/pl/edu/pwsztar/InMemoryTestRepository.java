package pl.edu.pwsztar;


import java.util.ArrayList;
import java.util.List;

public class InMemoryTestRepository implements AccountRepository{
    private static List<Account> accounts = new ArrayList<Account>();

    @Override
    public Account getByNumber(int accountNumber) throws AccountNonExistentException {
       return accounts.stream().
               filter(account -> account.getNumber()==accountNumber).
               findFirst()
               .orElseThrow(AccountNonExistentException::new);
    }

    @Override
    public void create(Account account) {
        accounts.add(account);
    }

    @Override
    public void delete(int accountNumber) throws AccountNonExistentException {
        accounts.remove(getByNumber(accountNumber));
    }

    @Override
    public List<Account> getAll() {
        return accounts;
    }
}
