package pl.edu.pwsztar;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;
    private final static InMemoryTestRepository repository = new InMemoryTestRepository();

    public int createAccount() {
        accountNumber++;
        repository.create(new Account(accountNumber));

        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        try{
            int balance = repository.getByNumber(accountNumber).getBalance();
            repository.delete(accountNumber);
            accountNumber--;
            return balance;
        }catch (AccountNonExistentException anee){
            return ACCOUNT_NOT_EXISTS;
        }
    }

    public boolean deposit(int accountNumber, int amount) {
        try {
            repository.getByNumber(accountNumber)
                    .deposit(amount);
            return true;
        }catch (AccountNonExistentException anee){
            return false;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        try {
            return repository.getByNumber(accountNumber)
                    .withdraw(amount);
        }catch (AccountNonExistentException anee){
            return false;
        }
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        try {
            return repository.getByNumber(fromAccount)
                    .transfer(repository.getByNumber(toAccount),amount);
        }catch (AccountNonExistentException anne){
            return false;
        }
    }

    public int accountBalance(int accountNumber){
        try{
            return repository.getByNumber(accountNumber).getBalance();
        }
        catch (AccountNonExistentException anee){
            return ACCOUNT_NOT_EXISTS;
        }
    }

    public int sumAccountsBalance() {
        return repository.getAll()
                .stream()
                .reduce(0,(total, account)-> total + account.getBalance(), Integer::sum);
    }
}
